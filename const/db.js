export default data = [
    {
        "id":1,
        "title": 'Pendaftaran Calas LabTI 2017',
        "image" : require('../src/img/daftar-1.jpg'),
        "body" : [
            'Laboratorium Teknik Informatika Universitas Gunadarma membuka kesempatan kepada mahasiswa Universitas Gunadarma Fakultas Teknologi Industri untuk bergabung sebagai Asisten Lab Teknik Informatika dengan persyaratan dan ketentuan sebagai berikut :\n\n',
            'Persyaratan:  \n',
            '\t 1. Mahasiswa Aktif Universitas Gunadarma\n\t 2. Minimal Semester 4 dengan IPK >= 2.75\n\t 3. Bersedia Menjadi Asisten Minimal 1 Tahun \n\t 4. Mengumpulkan Berkas Terlampir\n\t 5. Tidak Terikat Dengan Instansi Lain di Lingkungan Univ. Gunadarma.\n\n',
            'KELENGKAPAN YANG DIKUMPULKAN: \n',
            '\t 1. Curriculum  Vitae (CV) \n\t 2. Surat Lamaran \n\t 3. Print Out IPK Rangkuman\n\t 4. Fotocopy KTP (1 Lembar)\n\t 5. Pas Photo 3x4 Formal (2 Lembar) + (Softcopy)\n\t 6. Essay Diri (Min 400 Kata) + (Softcopy)\n\t 7. Sertifikat (Bila Ada) \n\t 8. Map Plastik Business File: \n\t\tTingkat 2 => Merah  \n\t\tTingkat 3 => Hijau  \n\t\tTingkat 4 => Biru\n\n',
            'Waktu Pendaftaran&Tempat Pengumpulan Berkas: \n 10 Juli 2018 s/d 11 Agustus 2018\n\n',
            'Pengumpulan Berkas:\n Gunadarma Kampus Kalimalang @J1222 \n\n',
            'CP: \n Kalimalang: \n M. Rezky : xxxx \n Silvia W : xxxx \n\n',
            ].join (''),
        "author" : 'Admin',
        "date": '25 May 2017'
    }
]