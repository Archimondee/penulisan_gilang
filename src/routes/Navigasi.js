import React from 'react';
import {Text, View} from 'react-native';
import {createBottomTabNavigator, createStackNavigator, createSwitchNavigator} from 'react-navigation';

import {Ionicons, MaterialIcons, Entypo} from '@expo/vector-icons';

import Login from '../Components/Login/Login';
import Auth from '../Components/Login/Auth';

import Home from '../Components/Profile/Home';
import Nilai from '../Components/Profile/Nilai';
import Profile from '../Components/Profile/Profile';
import ListNilai from '../Components/Profile/ListNilai';
import Page from '../Components/Profile/Page';

import PolicyScreen from '../Components/Policy/PolicyScreen';
import UsernamePolicy from '../Components/Policy/UsernamePolicy';
import AttendancePolicy from '../Components/Policy/AttendancePolicy';
import ChangePolicy from '../Components/Policy/ChangePolicy';
import ResetPolicy from '../Components/Policy/ResetPolicy';
import RulesPolicy from '../Components/Policy/RulesPolicy';
import WearPolicy from '../Components/Policy/WearPolicy';

const Policy = createStackNavigator({
  PolicyScreen: PolicyScreen,
  UsernamePolicy: UsernamePolicy,
  AttendancePolicy: AttendancePolicy,
  ChangePolicy:ChangePolicy,
  ResetPolicy:ResetPolicy,
  RulesPolicy:RulesPolicy,
  WearPolicy:WearPolicy
}, {
  initialRouteName: 'PolicyScreen',
  headerMode: 'none'
})
Policy.navigationOptions = ({navigation}) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
  };
};


const NilaiStack = createStackNavigator({
  NilaiScreen: Nilai,
  ListNilai: ListNilai
},{
  initialRouteName:'NilaiScreen',
  headerMode:'none'
})

NilaiStack.navigationOptions = ({navigation}) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
  };
}

const HomeStack = createStackNavigator({
  HomeScreen: Home,
  Page: Page
},{
  initialRouteName: 'HomeScreen',
  headerMode:'none'
})

HomeStack.navigationOptions = ({navigation}) => {
  let tabBarVisible = true;
  if (navigation.state.index > 0) {
    tabBarVisible = false;
  }
  return {
    tabBarVisible,
  };
}; 

Navigasi =  createBottomTabNavigator ({
  Home: HomeStack,
  Nilai: NilaiStack,
  Policy: Policy,
  Profile: Profile,
},{
    navigationOptions: ({ navigation }) => ({
      tabBarIcon: ({ focused, tintColor }) => {
        const { routeName } = navigation.state;
        let iconName;
        if (routeName === 'Home') {
          iconName = `ios-home${focused ? '' : '-outline'}`;
        } else if (routeName === 'Nilai') {
          iconName = `ios-archive${focused ? '' : '-outline'}`;
        } else if (routeName === 'Profile') {
          iconName = `ios-person${focused ? '' : '-outline'}`;
        }else if (routeName === 'Policy') {
          iconName = `ios-briefcase${focused ? '' : '-outline'}`;
        }

        // You can return any component that you like here! We usually use an
        // icon component from react-native-vector-icons
        return <Ionicons name={iconName} size={25} color={tintColor} />;
      },
    }),
    tabBarOptions: {
      activeTintColor: 'white',
      inactiveTintColor: '#ece5dd',
      style:{
        backgroundColor: '#075e54',
        
      }
    },
});

export default Main = createSwitchNavigator({
    Auth: Auth,
    Login: Login,
    Navigasi: Navigasi
},{
  initialRouteName:'Auth'
})