import React, { Component } from 'react';
import {Text, View, Image, ImageBackground, AsyncStorage, Alert, NetInfo, KeyboardAvoidingView} from 'react-native';
import { Container, Header, Content, Item, Input, Icon, Button, Label, Card, CardItem } from 'native-base';
import {Entypo} from '@expo/vector-icons';
import Loader from '../Loading/Loader';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

export default class Login extends Component {
    constructor(props){
        super(props);
        this.state={
            username: 'Guest',
            password: '',

            loading:false,
            connected: '',
            title:'Checking Your Connection',
            footer: 'Please Wait ....'
        }
        this.handleFirstConnectivityChange = this.handleFirstConnectivityChange.bind(this);
    }
    _UserLogin=()=>{
        const {username, password} = this.state;
        const load = true;
        this.setState({
            loading:true,
            title: 'Checking your username',
            footer: 'Please wait ...'
        })
        fetch('https://api-labti-gilangs.000webhostapp.com/api/Login.php',{
            method: 'POST',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
            },
            body: JSON.stringify({
                username: username,
                password: password
            })
        }).then((response)=>response.json())
            .then((responseJson)=>{
                if(responseJson === 'Logged In'){
                    this._signInAsync();
                    setTimeout(()=>{
                        this.setState({
                            loading:true,
                            title: 'Logged In',
                            footer: 'Please wait ...'
                        })
                    }, 2500)
                    if(load){
                        setTimeout(()=>{
                            this.setState({
                                loading: false
                            })
                            this.props.navigation.navigate('Home');
                        }, 3500)
                    }
                }else{
                    setTimeout(() => {
                        this.setState({
                            loading: true,
                            title: 'Wrong Username or Password',
                            footer: 'Try Again .....'
                        })
                    }, 2500)
                    if(load){
                        setTimeout(() => {
                            this.setState({
                                loading: false
                            })
                        }, 3500)
                    }
                }
            }).catch((error)=>{
                console.error(error);
            })
    }
    _UserGuest=()=>{
        setTimeout(()=>{
            this.setState({
                loading:true,
                title: 'Login As Guest',
                footer: 'Please wait ...'
            })
            this.props.navigation.navigate('Home');
        },2500)
        
    }
    _signInAsync = async () => {
        await AsyncStorage.setItem('username', this.state.username);
        await AsyncStorage.setItem('password', this.state.password);
    }
    _signAsGuest = async () => {
        await AsyncStorage.setItem('username', this.state.username);
    }
    componentDidMount() {
        NetInfo.isConnected.fetch().then((isConnected) => {
            console.log('First, is ' + (isConnected ? 'online' : 'offline'));
        })

        NetInfo.isConnected.addEventListener(
            'connectionChange',
            this.handleFirstConnectivityChange
        );

    }
    handleFirstConnectivityChange(isConnected) {
        console.log('Then, is ' + (isConnected ? 'online' : 'offline'));
        if (isConnected == true) {
            setTimeout(() => {
                this.setState({
                    loading: false,
                })
            }, 2500)
        } else {
            setTimeout(() => {
                this.setState({
                    loading: true,
                })
            }, 2500)
        }
    };
    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this.handleFirstConnectivityChange
        );
    };
  render() {
    return (
        
        <Container style={{}}>
            
                <KeyboardAvoidingView style={{width:'100%', height:'100%', backgroundColor:'white'}} behavior="padding" enabled>
                <ImageBackground source={require('../../../assets/bg.jpg')} style={{height:"100%", width:"100%", flex:1}} resizeMode='cover'>
                <Content padder>
                        <Loader
                            loading={this.state.loading}
                            title={this.state.title}
                            footer={this.state.footer}
                        />
                    <View style={{justifyContent:'center', alignContent:'center', alignItems:'center', marginTop:50}}>
                        <Image source={require('../../../assets/logo.jpg')} style={{width:"76%", resizeMode:'contain'}}/>
                    </View>
                    <Card style={{marginLeft:30, marginRight:30, marginTop:70}}>
                        <View style={{marginLeft:20, marginRight:20, marginTop:30, marginBottom:30}}>
                            <Item style={{borderBottomColor:'black'}}>
                                <Entypo name='user' size={20} color='black'/>
                                <Input placeholder='Username' placeholderTextColor='#000' style={{color:'black'}}
                                    onChangeText={username => this.setState({username})}
                                    
                                />
                            </Item>
                            <Item style={{borderBottomColor:'black'}}>
                                <Entypo name='lock' size={20} color='black'/>
                                <Input placeholder='Password' placeholderTextColor='#000' style={{color:'black'}}
                                    onChangeText={password => this.setState({password})}
                                    secureTextEntry={true}
                                />
                            </Item>
                            <View style={{marginTop:20, flexDirection:'column'}}>
                                <Button full style={{width:'100%', backgroundColor:'#075e54'}} onPress={this._UserLogin}>
                                    <Text style={{color:'white'}}>Login</Text>
                                </Button>
                                <Button full style={{width:'100%', backgroundColor:'#075e54', marginTop:10}} onPress={this._UserGuest}>
                                    <Text style={{color:'white'}}>Login As Guest</Text>
                                </Button>
                            </View> 
                        </View>                    
                    </Card>
                </Content>
            </ImageBackground>
            </KeyboardAvoidingView>
        </Container>
    );
  }
}