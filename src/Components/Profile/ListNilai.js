import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
} from 'react-native';
import {
  Content,
  Container,
  Card,
  CardItem,
  Body,
  Left,
  Header,
  Right,
} from 'native-base';
import {Ionicons} from '@expo/vector-icons';


export default class NilaiScreen extends Component {
  constructor (props) {
    super (props);
    this.state = {
      nm_ast: '',
      nilai_praktikum: [],
      id_ast: this.props.navigation.getParam ('id_asisten'),
      npm_mhs: this.props.navigation.getParam ('npm_mhs'),
      id_praktikum: this.props.navigation.getParam ('id_praktikum'),
    };
  }
  componentDidMount = () => {
    const {id_ast} = this.state;
    fetch ('http://api-labti-gilangs.000webhostapp.com/api/GetName.php', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify ({
        id_ast: id_ast,
      }),
    })
      .then (response => response.json ())
      .then (responseJson => {
        this.setState ({
          nm_ast: responseJson[0].nm_ast,
        });
      })
      .catch (error => {
        console.error (error);
      });

    const {npm_mhs} = this.state;
    const {id_praktikum} = this.state;

    fetch ('http://api-labti-gilangs.000webhostapp.com/api/ShowOnePelajaran.php', {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify ({
        npm_mhs: npm_mhs,
        id_praktikum: id_praktikum,
      }),
    })
      .then (response => response.json ())
      .then (responseJson => {
        this.setState ({
          nilai_praktikum: responseJson,
        });
      })
      .catch (error => {
        console.error (error);
      });
  };
  componentWillUpdate = () => {};

  render () {
    const datas = this.state.nilai_praktikum;

    return (
      
        <Container>
        <ImageBackground source={require('../../../assets/bg.jpg')} style={{height:"100%", width:"100%"}} resizeMode='cover'>
        <Header style={{backgroundColor:'#075e54', marginTop:StatusBar.currentHeight
        }}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={{marginTop:7}}>
                <Left>
                    <Ionicons name="ios-arrow-back" size={40} color="white"/>
                </Left>
            </TouchableOpacity>
            <Body style={{marginLeft:10}}>
                <Text style={{color:'white', fontWeight:'bold'}}>{this.state.id_praktikum}</Text>
            </Body>
        </Header>
          <Content padder>
            <Card transparent style={{flex: 1}}>
              <CardItem
                header
                style={{
                  borderWidth: 1,
                  backgroundColor: '#075e54',
                  justifyContent: 'center',
                  alignContent: 'center',
                }}
              >
                <Text style={{color:'white'}}>Informasi Praktikum</Text>
              </CardItem>
              <CardItem
                style={{flexDirection: 'column', alignItems: 'flex-start'}}
              >
                <Text>
                  {' '}
                  Nama Praktikum :
                  {' '}
                  {this.props.navigation.getParam ('nm_praktikum')}
                </Text>
                <Text>
                  {' '}
                  Kelas Praktikum :
                  {' '}
                  {this.props.navigation.getParam ('kls_praktikum')}
                  {' '}
                </Text>
                <Text> Penanggung Jawab : {this.state.nm_ast}</Text>
              </CardItem>

              <CardItem
                header
                style={{
                  marginTop: 20,
                  borderWidth: 1,
                  backgroundColor: '#075e54',
                  justifyContent: 'center',
                  alignContent: 'center',
                }}
              >
                <Text style={{color:'white'}}>Nilai Praktikum</Text>
              </CardItem>
              <CardItem style={{justifyContent:'center', alignContent:'center', alignItems:'center', alignSelf:'center', flexDirection:'column'}}>
                  <View
                    style={{flexDirection: 'row', marginTop: 10, borderWidth: 1, marginLeft:2.5}}
                  >
                    <View
                      style={{
                        flexWrap: 'wrap',
                        height: 50,
                        width: 70,
                        justifyContent: 'center',
                      }}
                    >
                      <Text style={{textAlign: 'center'}}> Pertemuan </Text>
                    </View>

                    <View
                      style={{
                        flexWrap: 'wrap',
                        height: 50,
                        width: 70,
                        borderLeftWidth: 1,
                        justifyContent: 'center',
                      }}
                    >
                      <Text style={{textAlign: 'center'}}> Nilai LA </Text>
                    </View>

                    <View
                      style={{
                        flexWrap: 'wrap',
                        height: 50,
                        width: 70,
                        borderLeftWidth: 1,
                        justifyContent: 'center',
                      }}
                    >
                      <Text style={{textAlign: 'center'}}> Nilai LP </Text>
                    </View>

                    <View
                      style={{
                        flexWrap: 'wrap',
                        height: 50,
                        width: 70,
                        borderLeftWidth: 1,
                        justifyContent: 'center',
                      }}
                    >
                      <Text style={{textAlign: 'center'}}> Nilai TP </Text>
                    </View>

                    <View
                      style={{
                        flexWrap: 'wrap',
                        height: 50,
                        width: 70,
                        borderLeftWidth: 1,
                        justifyContent: 'center',
                      }}
                    >
                      <Text style={{textAlign: 'center'}}> Nilai K </Text>
                    </View>
                  </View>
                    <View style={{flexDirection: 'column',marginLeft:2.5, marginBottom:10}}>
                      {this.state.nilai_praktikum.map ((items, i) => {
                        return (
                          <View key={i} style={{flexDirection: 'row'}}>
                            <View
                              style={{
                                height: 50,
                                width: 70,

                                justifyContent: 'center',
                                borderLeftWidth: 1,
                                borderBottomWidth: 1,
                              }}
                            >
                              <Text style={{textAlign: 'center'}}>
                                {' '}{items.pert}{' '}
                              </Text>
                            </View>
                            <View
                              style={{
                                height: 50,
                                width: 70,

                                justifyContent: 'center',
                                borderLeftWidth: 1,
                                borderBottomWidth: 1,
                              }}
                            >
                              <Text style={{textAlign: 'center'}}>
                                {' '}{items.nilai_la}{' '}
                              </Text>
                            </View>
                            <View
                              style={{
                                height: 50,
                                width: 70,

                                justifyContent: 'center',
                                borderLeftWidth: 1,
                                borderBottomWidth: 1,
                              }}
                            >
                              <Text style={{textAlign: 'center'}}>
                                {' '}{items.nilai_lp}{' '}
                              </Text>
                            </View>
                            <View
                              style={{
                                height: 50,
                                width: 70,

                                justifyContent: 'center',
                                borderLeftWidth: 1,
                                borderBottomWidth: 1,
                              }}
                            >
                              <Text style={{textAlign: 'center'}}>
                                {' '}{items.nilai_tp}{' '}
                              </Text>
                            </View>
                            <View
                              style={{
                                height: 50,
                                width: 70,

                                justifyContent: 'center',
                                borderLeftWidth: 1,
                                borderBottomWidth: 1,
                                borderRightWidth: 1,
                              }}
                            >
                              <Text style={{textAlign: 'center'}}>
                                {' '}{items.nilai_k}{' '}
                              </Text>
                            </View>
                          </View>
                        );
                      })}
                    </View>
              </CardItem>
              
            </Card>
          </Content>
          </ImageBackground>
        </Container>
      
      
    );
  }
}
