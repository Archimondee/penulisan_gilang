import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  Image,
  NetInfo,
  AsyncStorage,
  StatusBar,
  ImageBackground,
  Alert
} from 'react-native';
import {
  Container,
  Header,
  Content,
  Label,
  Card,
  CardItem,
  Body,
  Left,
  Right,
  Title,
  Item,
  Input,
  Button
} from 'native-base';
import {Ionicons, MaterialIcons, Entypo, EvilIcons, SimpleLineIcons} from '@expo/vector-icons';
import {ImagePicker} from 'expo';


export default class Profile extends Component {
    constructor(props){
        super(props);
        this.state={
            username: '',
            nama:'',
            npm:'',
            kelas: '',
            image: null,

            user:'',
            password:'',

            name:'',

            password_pertama: '',
            password_kedua: '',

            password_baru:'',
            password_konfirmasi: '',
        }
    }
    _signOutAsync = async ()=>{
        await AsyncStorage.clear();
        this.props.navigation.navigate('Login');
    }
    _UserLogin=()=>{
        const {user, password} = this.state;
        fetch('https://api-labti-gilangs.000webhostapp.com/api/Login.php',{
            method: 'POST',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
            },
            body: JSON.stringify({
                username: user,
                password: password
            })
        }).then((response)=>response.json())
            .then((responseJson)=>{
                if(responseJson === 'Logged In'){
                    this._signInAsync();
                    AsyncStorage.setItem('password',password);
                    this.props.navigation.navigate('Home')
                    this.setState({
                      username:user
                    })
                    AsyncStorage.setItem('username', user);
                    this._getUsername();
                    
                }else{
                    console.log('Password Salah')
                }
            }).catch((error)=>{
                console.error(error);
            })
    }
    _signInAsync = async () => {
        await AsyncStorage.setItem('username', this.state.username);
    }
    _getUsername(){
        if(this.state.username !== 'Guest'){
          AsyncStorage.getItem("username").then((value) => {
          this.setState({"username": value});
            fetch('https://api-labti-gilangs.000webhostapp.com/api/GetProfile.php', {
                method: 'POST',
                headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    },
                        body: JSON.stringify({
                        username_mhs: value
                    })
                }).then((response)=>response.json())
                    .then((responseJson)=>{
                        this.setState({
                            nama: responseJson[0].nama_mhs || '',
                            npm: responseJson[0].npm_mhs || '',
                            kelas:responseJson[0].kls_praktikum || '',
                            image: responseJson[0].image || null
                        })
                        let Profile = {
                          nama : responseJson[0].nama_mhs || '',
                          npm : responseJson[0].npm_mhs || '',
                          kelas: responseJson[0].kls_praktikum || '',
                          image: responseJson[0].image || null
                        }
                        AsyncStorage.setItem('Profile', JSON.stringify(Profile));
                    })
            });
        }
    }

    _ChangePassword=()=>{
        AsyncStorage.getItem('password').then((value)=>{
            this.setState({
                password_pertama: value
            })
            
        });
       const {username, password_pertama, password_kedua, password_konfirmasi, password_baru} = this.state;
       var benar = 0;
       if(password_kedua !== ''){
           benar += 1
       }
       if(password_pertama === password_kedua){
           benar += 1
       }
       if(password_baru !== ''){
           benar += 1
       }
       if(password_konfirmasi !== ''){
           benar += 1
       }
       if(password_baru === password_konfirmasi){
           benar += 1
       }
       if(benar === 5){
           fetch('http://api-labti-gilangs.000webhostapp.com/api/ChangePassword.php',{
               method: 'POST',
               headers: {
                   'Accept' : 'application/json',
                   'Content-Type' : 'application/json',
               },
               body: JSON.stringify({
                   username: username,
                   new_password: password_baru
               })
           }).then((response)=>response.json())
                .then((responseJson)=>{
                    if(responseJson === 'Password berhasil di ganti'){
                        Alert.alert(responseJson)
                        this._signOutAsync();
                    }else{
                        Alert.alert('Galat terjadi');
                    }
                })
       }else{
           Alert.alert('Ada kesalahan');
       }
    }

    _pickImage = async () =>{
        let result = await ImagePicker.launchImageLibraryAsync({
            allowsEditing: true,
            aspect: [4,4],
            base64: true
        })

        if(!result.cancelled){
            let test = result.uri;
            let hasil = test.substring(test.lastIndexOf('.')+1);
            let username = this.state.username;

            fetch('http://api-labti-gilangs.000webhostapp.com/api/ChangeImage.php',{
            method: 'POST',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                base64: result.base64,
                tipe: hasil,
                username: username
                })  
            }).then((response)=>response.json())
                .then((responseJson)=>{
                    alert(responseJson);
                })

            let test_base64 = 'data:image/'+hasil+';base64,'+result.base64;
            let Profile = {
                nama : this.state.nama || '',
                npm : this.state.npm || '',
                kelas: this.state.kelas || '',
                image: test_base64 || null
            }
                AsyncStorage.setItem('Profile', JSON.stringify(Profile));
            this.setState({image: test_base64});
            
        }
    }

    componentWillMount(){
        this._getUsername();
        AsyncStorage.getItem('Profile').then((value)=>{
            let data = JSON.parse(value);
            this.setState({
                image: data.image || null
            })
        })
    }
  render() {
      
    return (
        
            <Container>
            <ImageBackground source={require('../../../assets/bg.jpg')} style={{height:"100%", width:"100%"}} resizeMode='cover'>
        <Content padder>
        {
            this.state.username ? (
                <View>
                    <View style={{marginTop:StatusBar.currentHeight}}>

                        {
                            this.state.image && <Image
                            source={{uri: this.state.image}}
                            style={{width: '100%', height: 250}}
                            resizeMode="contain"
                            />
                        }
                        <View style={{marginTop: 20, alignContent:'center', justifyContent:'center', alignItems:'center', alignSelf:'center'}}>
                            <Button 
                                onPress={this._pickImage}
                            style={{backgroundColor:'#075e54', width:260, justifyContent:'center', alignContent:'center', alignItems:'center', borderWidth:0.5, borderColor:'white'}}>
                                <Text style={{color:'white', textAlign:'center'}}>Change Profile Picture</Text>
                            </Button>
                        </View>
                        
                    </View>
                    <Card style={{marginTop:50, marginBottom:50}}>
                        <CardItem header bordered style={{justifyContent:'center', alignContent:'center', alignItems:'center', backgroundColor:'#075e54'}}>
                            <Text style={{color:'white'}}>Your Information</Text>
                        </CardItem>
                        <CardItem>
                            <Item>
                                <SimpleLineIcons active name='info' size={18}/>
                                <Input placeholder={this.state.npm} disabled style={{marginLeft:10}}/>
                            </Item>
                        </CardItem>
                        <CardItem>
                            <Item>
                                <SimpleLineIcons active name='user' size={18}/>
                                <Input placeholder={this.state.nama} disabled style={{marginLeft:10}}/>
                            </Item>
                        </CardItem>
                        <CardItem>
                            <Item>
                                <SimpleLineIcons active name='organization' size={18}/>
                                <Input placeholder={this.state.kelas} disabled style={{marginLeft:10}}/>
                            </Item>
                        </CardItem>
                        <CardItem>
                            <Item>
                                <SimpleLineIcons active name='user' size={18}/>
                                <Input placeholder={this.state.username} disabled style={{marginLeft:10}}/>
                            </Item>
                        </CardItem>
                        <CardItem>
                            <Item>
                                <SimpleLineIcons active name='lock-open' size={18}/>
                                <Input 
                                    onChangeText={password_kedua => this.setState({password_kedua})}
                                    placeholder='Current Password' style={{marginLeft:10}}
                                    secureTextEntry={true}/>
                            </Item>
                        </CardItem>
                        <CardItem>
                            <Item>
                                <SimpleLineIcons active name='lock' size={18}/>
                                <Input 
                                onChangeText={password_baru => this.setState({password_baru})}
                                placeholder='New Password' style={{marginLeft:10}}
                                secureTextEntry={true}/>
                            </Item>
                        </CardItem>
                        <CardItem>
                            <Item>
                                <SimpleLineIcons active name='lock' size={18}/>
                                <Input 
                                onChangeText={password_konfirmasi => this.setState({password_konfirmasi})}
                                placeholder='Confirmation Password' style={{marginLeft:10}}
                                secureTextEntry={true}/>
                            </Item>
                        </CardItem>
                        <View style={{flexDirection:'column'}}>
                            <Button full 
                                onPress={this._ChangePassword}
                            style={{marginTop:20, marginBottom:10, marginLeft:20, marginRight:20, backgroundColor:'#075e54'}}>
                                <Text style={{color:'white'}}>Save</Text>
                            </Button>
                            <Button full 
                                onPress={this._signOutAsync}
                                style={{marginTop:10, marginBottom:20, marginLeft:20, marginRight:20, backgroundColor:'#075e54'}}>
                                <Text style={{color:'white'}}>Logout</Text>
                            </Button>    
                        </View>
                                    
                    </Card>
                </View>
                
            ) : (<View>
      <View
        style={{
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
          marginTop: 50,
        }}
      >
        <Image
          source={require ('../../../assets/logo.jpg')}
          style={{width: '76%', resizeMode: 'contain'}}
        />
      </View>
      <Card style={{marginLeft: 30, marginRight: 30, marginTop: 70}}>
        <View
          style={{
            marginLeft: 20,
            marginRight: 20,
            marginTop: 30,
            marginBottom: 30,
          }}
        >
          <Item style={{borderBottomColor: 'black'}}>
            <Entypo name="user" size={20} color="black" />
            <Input
              placeholder="Username"
              placeholderTextColor="#000"
              style={{color: 'black'}}
              onChangeText={user => this.setState ({user})}
            />
          </Item>
          <Item style={{borderBottomColor: 'black'}}>
            <Entypo name="lock" size={20} color="black" />
            <Input
              placeholder="Password"
              placeholderTextColor="#000"
              style={{color: 'black'}}
              onChangeText={password => this.setState ({password})}
              secureTextEntry={true}
            />
          </Item>
          <View style={{marginTop: 20}}>
            <Button
              full
              style={{width: '100%', backgroundColor: '#075e54'}}
              onPress={this._UserLogin}
            >
              <Text style={{color: 'white'}}>Login</Text>
            </Button>
          </View>
        </View>
      </Card>
    </View>)
        }
            
        </Content>
        </ImageBackground>
      </Container>
        
      
    );
  }
}
