import React, {Component} from 'react';
import {
  Container,
  Header,
  Content,
  Card,
  CardItem,
  Body,
  Right,
  Left,
  Title,
  Button,
} from 'native-base';

import {
  View,
  ScrollView,
  Image,
  Alert,
  TouchableOpacity,
  Text,
  AsyncStorage,
  StatusBar,
  ImageBackground,
} from 'react-native';
import {Ionicons} from '@expo/vector-icons';
import data from '../../../const/db';

export default class Home extends Component {
  constructor(props){
        super(props);
        this.state={
            username: '',
            nama:'',
            npm:'',
            kelas: '',

            data: data,
            image:null
        }
    }
  _getUsername(){
      if(this.state.username !== 'Guest'){
          AsyncStorage.getItem("username").then((value) => {
          this.setState({"username": value});
          //console.log(value);
            fetch('https://api-labti-gilangs.000webhostapp.com/api/GetProfile.php', {
                method: 'POST',
                headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    },
                        body: JSON.stringify({
                        username_mhs: value
                    })
                }).then((response)=>response.json())
                    .then((responseJson)=>{
                        this.setState({
                            nama: responseJson[0].nama_mhs || '',
                            npm: responseJson[0].npm_mhs || '',
                            kelas:responseJson[0].kls_praktikum || '',
                            image: responseJson[0].image || '',
                        })
                        let Profile = {
                          nama : responseJson[0].nama_mhs || '',
                          npm : responseJson[0].npm_mhs || '',
                          kelas: responseJson[0].kls_praktikum || '',
                          image: responseJson[0].image || '',
                        }
                        AsyncStorage.setItem('Profile', JSON.stringify(Profile));
                    })
            });
        }
    }
    _findNpm(){
        const npm = this.state.npm;
        return fetch('https://api-labti-gilangs.000webhostapp.com/api/ShowOnePraktikum.php', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            npm_mhs: npm
        })
        }).then((response) => response.json())
        .then((responseJson) => {
            this.setState({
            praktikum: responseJson
            });
        }).catch((error) => {
            console.error(error);
        });
    }
    componentWillMount(){
         this._getUsername();
    }
  render() {
    return (
      
            <Container>
            <ImageBackground source={require('../../../assets/bg.jpg')} style={{height:"100%", width:"100%"}} resizeMode='cover'>
            <Content padder style={{marginTop:StatusBar.currentHeight}}>
            <ScrollView horizontal={true}>
                {
                    data.map((items, i)=>{
                        return (
                            <TouchableOpacity 
                                key={items.id} 
                                onPress={()=>this.props.navigation.navigate('Page',{
                                    body: items.body, image: items.image, title: items.title })}>
                                <Card style={{height:200, width:300, backgroundColor:'white', paddingLeft:10}}>
                                    <Image source={items.image}
                                        style={{height:100, width:'100%'}}
                                    />
                                    <View style={{flexWrap:'wrap', flexGrow:1, marginLeft:5, marginRight:5, marginTop:10}}>
                                        <Text style={{fontSize:16, fontWeight:'bold'}}>
                                            {items.title}
                                        </Text>
                                    </View>
                                    <View 
                                    style={{flexDirection:'column', marginTop:5, marginLeft:5, marginRight:5, marginBottom:15}}>
                                        <Text style={{fontSize:14}}>Written By :  {items.author} </Text>
                                        <Text style={{color:'gray', fontSize:14}}>{items.date}</Text>
                                    </View>
                                </Card>
                            </TouchableOpacity>
                        )
                    })
                }
            </ScrollView>
            <View style={{backgroundColor:'white', marginTop:10}}>
                <Card>
                    <CardItem header style={{backgroundColor:'#075e54', justifyContent:'center', alignContent:'center'}}>
                        <Text style={{color:'white'}}>Informasi Lab TI</Text>
                    </CardItem>
                </Card>
                    {
                        data.map((items,i)=>{
                            return(
                                <TouchableOpacity key={items.id}>
                                    <Card>
                                        <View style={{height:170}}>
                                            <View style={{flexDirection:'row'}}>
                                                <View style={{width:'60%', flexWrap:'wrap', flexGrow:1, marginLeft:10, marginTop:10}}>
                                                    <Text style={{fontWeight:'bold', fontSize:20}}>{items.title} </Text>
                                                    <View style={{flexDirection:'column', marginTop:40}}>
                                                        <Text>
                                                            Written By {items.author}
                                                        </Text>
                                                        <Text>
                                                            {items.date}
                                                        </Text>
                                                    </View>
                                                </View>
                                                <View style={{width:150, height:150, marginRight:15, marginTop:10, marginBottom:10}}>
                                                    <Image source={items.image} 
                                                    style={{width:"100%", height:"100%", resizeMode:'contain'}}
                                                    />
                                                </View>
                                            </View>                    
                                        </View>
                                    </Card>
                                </TouchableOpacity>
                            )
                        })
                    }
            </View>
              
            </Content>
            </ImageBackground>
        </Container>
        
    );
  }
}
