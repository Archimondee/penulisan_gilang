import React, {Component} from 'react';
import {View, Text, StyleSheet, Image, AsyncStorage, ImageBackground, StatusBar, NetInfo} from 'react-native';
import {
  Container,
  Header,
  Content,
  Label,
  Card,
  CardItem,
  Body,
  Left,
  Right,
  Button,
  Item,
  Input,
  Title,
} from 'native-base';
import {Ionicons, Entypo} from '@expo/vector-icons';
import Loader from '../Loading/Loader';


export default class Nilai extends Component {
  constructor(props){
    super(props);
    this.state={
      praktikum: [],
      username: '',
      nama:'',
      npm:'',
      kelas:'',

      user: '',
      password: '',

      name: '',
      image: null,

      loading:false,
      connected: '',
      title: 'Checking your connection',
      footer: 'Please wait ...'
    }
    this.handleFirstConnectivityChange = this.handleFirstConnectivityChange.bind(this);
  }
  _ListPraktikum(){
    //console.log('Dijalankan');
    if(this.state.username !== 'Guest'){
      AsyncStorage.getItem('Profile').then((value)=>{
      let data = JSON.parse(value);
      if(data !== null){
        fetch('http://api-labti-gilangs.000webhostapp.com/api/ShowOnePraktikum.php',{
          method: 'POST',
          headers: {
              'Accept' : 'application/json',
              'Content-Type' : 'application/json'
          },
          body: JSON.stringify({
              npm_mhs: data.npm
          })
            }).then((response)=>response.json())
                .then((responseJson)=>{
                this.setState({
                  praktikum: responseJson
                });
                //console.log('Praktikum : ', this.state.praktikum);
          }).catch((error)=>{
              console.error(error);
          });
        }  
      })
    }
  }
  

  _UserLogin=()=>{
        const {user, password} = this.state;
        const load = true;
        this.setState({
            loading: true,
            title: 'Checking your username',
            footer: 'Please wait .....'
        })
        fetch('https://api-labti-gilangs.000webhostapp.com/api/Login.php',{
            method: 'POST',
            headers: {
                'Accept' : 'application/json',
                'Content-Type' : 'application/json',
            },
            body: JSON.stringify({
                username: user,
                password: password
            })
        }).then((response)=>response.json())
            .then((responseJson)=>{
                if(responseJson === 'Logged In'){
                    this._signInAsync();
                    AsyncStorage.setItem('password',password);
                    setTimeout(()=>{
                      this.setState({
                        loading:true,
                        title: 'Logged in',
                        footer: 'Please wait ...'
                      })
                    },2500)
                    if(load){
                      setTimeout(()=>{
                        this.setState({
                          loading: false
                        })
                        this.props.navigation.navigate('Home')
                        this.setState({
                          username:user
                        })
                        AsyncStorage.setItem('username', user);
                        this._getUsername();
                      },3500)
                    }  
                }else{
                    setTimeout(() => {
                        this.setState({
                            loading: true,
                            title: 'Wrong Username or Password',
                            footer: 'Try Again .....'
                        })
                    }, 2500)
                    if(load){
                        setTimeout(() => {
                            this.setState({
                                loading: false
                            })
                        }, 3500)
                    }
                }
            }).catch((error)=>{
                console.error(error);
            })
    }
    handleFirstConnectivityChange(isConnected) {
        console.log('Then, is ' + (isConnected ? 'online' : 'offline'));
        if (isConnected == true) {
            setTimeout(() => {
                this.setState({
                    loading: false,
                })
            }, 2500)
        } else {
            setTimeout(() => {
                this.setState({
                    loading: true,
                })
            }, 2500)
        }
    };
    componentWillUnmount() {
        NetInfo.isConnected.removeEventListener(
            'connectionChange',
            this.handleFirstConnectivityChange
        );
    };
     _getUsername(){
      if(this.state.username !== 'Guest'){
          AsyncStorage.getItem("username").then((value) => {
          this.setState({"username": value});
            fetch('https://api-labti-gilangs.000webhostapp.com/api/GetProfile.php', {
                method: 'POST',
                headers: {
                      'Accept': 'application/json',
                      'Content-Type': 'application/json'
                    },
                        body: JSON.stringify({
                        username_mhs: value
                    })
                }).then((response)=>response.json())
                    .then((responseJson)=>{
                        this.setState({
                            nama: responseJson[0].nama_mhs || '',
                            npm: responseJson[0].npm_mhs || '',
                            kelas:responseJson[0].kls_praktikum || '',
                            image: responseJson[0].image || null
                        })
                        let Profile = {
                          nama : responseJson[0].nama_mhs || '',
                          npm : responseJson[0].npm_mhs || '',
                          kelas: responseJson[0].kls_praktikum || '',
                          image: responseJson[0].image || null
                        }
                        AsyncStorage.setItem('Profile', JSON.stringify(Profile));
                        this._ListPraktikum();
                    })
            });
        }
    }
    _signInAsync = async () => {
        await AsyncStorage.setItem('username', this.state.username);
        //await AsyncStorage.setItem('password', this.state.password);
    }

  componentWillMount() {
    AsyncStorage.getItem('username').then((value)=>{
      this.setState({
        username: value
      })
    });
    this._ListPraktikum();
        NetInfo.isConnected.fetch().then((isConnected) => {
            console.log('First, is ' + (isConnected ? 'online' : 'offline'));
        })

        NetInfo.isConnected.addEventListener(
            'connectionChange',
            this.handleFirstConnectivityChange
        );
  }
  renderTingkat1() {
    const AllData = this.state.praktikum;
    const tingkat1 = [];
    AllData.map((items) => {
      if (items.tingkat === '1' ) {
        tingkat1.push(
          items,
        )
      }
    });
    //console.log(tingkat1);
    return tingkat1;
  }

  renderTingkat2() {
    const AllData = this.state.praktikum;
    const tingkat2 = [];
    AllData.map((items) => {
      if (items.tingkat === '2' ) {
        tingkat2.push(
          items,
        )
      }
    });
    //console.log(tingkat1);
    return tingkat2;
  }

  renderTingkat3() {
    const AllData = this.state.praktikum;
    const tingkat3 = [];
    AllData.map((items) => {
      if (items.tingkat === '3' ) {
        tingkat3.push(
          items,
        )
      }
    });
    //console.log(tingkat1);
    return tingkat3;
  }

  renderTingkat4() {
    const AllData = this.state.praktikum;
    const tingkat4 = [];
    AllData.map((items) => {
      if (items.tingkat === '4' ) {
        tingkat4.push(
          items,
        )
      }
    });
    //console.log(tingkat1);
    return tingkat4;
  }
  render() {
    
const tingkatSatu = this.renderTingkat1 ();
const CardSatu = tingkatSatu.length > 0
  ? <Card
      renderHeader={() => (
        <CardItem
          header
          style={{
            backgroundColor: '#075e54',
            borderWidth: 0.5,
            borderColor: 'gray',
          }}
        >
          <Text style={{color: 'white'}}> Tingkat 1</Text>
        </CardItem>
      )}
      dataArray={tingkatSatu}
      renderRow={tingkatSatu => (
        <CardItem
          button
          onPress={() =>
            this.props.navigation.navigate ('ListNilai', {
              npm_mhs: tingkatSatu.npm_mhs,
              id_praktikum: tingkatSatu.id_praktikum,
              nm_praktikum: tingkatSatu.nm_praktikum,
              id_asisten: tingkatSatu.id_asisten,
              kls_praktikum: tingkatSatu.kls_praktikum,
            })}
        >
          <Left>
            <Text>{tingkatSatu.id_praktikum}</Text>
          </Left>
          <Right>
            <Ionicons name="ios-arrow-dropright-outline" size={32} />
          </Right>
        </CardItem>
      )}
    />
  : null;

const tingkatDua = this.renderTingkat2 ();
const CardDua = tingkatDua.length > 0
  ? <Card
      renderHeader={() => (
        <CardItem
          header
          style={{
            backgroundColor: '#075e54',
            borderWidth: 0.5,
            borderColor: 'gray',
          }}
        >
          <Text style={{color: 'white'}}> Tingkat 2</Text>
        </CardItem>
      )}
      dataArray={tingkatDua}
      renderRow={tingkatDua => (
        <CardItem
          button
          onPress={() =>
            this.props.navigation.navigate ('ListNilai', {
              npm_mhs: tingkatDua.npm_mhs,
              id_praktikum: tingkatDua.id_praktikum,
              nm_praktikum: tingkatDua.nm_praktikum,
              id_asisten: tingkatDua.id_asisten,
              kls_praktikum: tingkatDua.kls_praktikum,
            })}
        >
          <Left>
            <Text>{tingkatDua.id_praktikum}</Text>
          </Left>
          <Right>
            <Ionicons name="ios-arrow-dropright-outline" size={32} />
          </Right>
        </CardItem>
      )}
    />
  : null;

const tingkatTiga = this.renderTingkat3 ();
const CardTiga = tingkatTiga.length > 0
  ? <Card
      renderHeader={() => (
        <CardItem
          header
          style={{
            backgroundColor: '#075e54',
            borderWidth: 0.5,
            borderColor: 'gray',
          }}
        >
          <Text style={{color: 'white'}}> Tingkat 3</Text>
        </CardItem>
      )}
      dataArray={tingkatTiga}
      renderRow={tingkatTiga => (
        <CardItem
          button
          onPress={() =>
            this.props.navigation.navigate ('ListNilai', {
              npm_mhs: tingkatTiga.npm_mhs,
              id_praktikum: tingkatTiga.id_praktikum,
              nm_praktikum: tingkatTiga.nm_praktikum,
              id_asisten: tingkatTiga.id_asisten,
              kls_praktikum: tingkatTiga.kls_praktikum,
            })}
        >
          <Left>
            <Text>{tingkatTiga.id_praktikum}</Text>
          </Left>
          <Right>
            <Ionicons name="ios-arrow-dropright-outline" size={32} />
          </Right>
        </CardItem>
      )}
    />
  : null;

const TingkatEmpat = this.renderTingkat4 ();
const CardEmpat = TingkatEmpat.length > 0
  ? <Card
      renderHeader={() => (
        <CardItem
          header
          style={{
            backgroundColor: '#075e54',
            borderWidth: 0.5,
            borderColor: 'gray',
          }}
        >
          <Text style={{color: 'white'}}> Tingkat 3</Text>
        </CardItem>
      )}
      dataArray={TingkatEmpat}
      renderRow={TingkatEmpat => (
        <CardItem
          button
          onPress={() =>
            this.props.navigation.navigate ('ListNilai', {
              npm_mhs: TingkatEmpat.npm_mhs,
              id_praktikum: TingkatEmpat.id_praktikum,
              nm_praktikum: TingkatEmpat.nm_praktikum,
              id_asisten: TingkatEmpat.id_asisten,
              kls_praktikum: TingkatEmpat.kls_praktikum,
            })}
        >
          <Left>
            <Text>{TingkatEmpat.id_praktikum}</Text>
          </Left>
          <Right>
            <Ionicons name="ios-arrow-dropright-outline" size={32} />
          </Right>
        </CardItem>
      )}
    />
  : null;

    return (
      
        <Container>
        <ImageBackground source={require('../../../assets/bg.jpg')} style={{height:"100%", width:"100%"}} resizeMode='cover'>
          <Content style={{marginTop:StatusBar.currentHeight, }}>
          <Loader
            loading={this.state.loading}
            title={this.state.title}
            footer={this.state.footer}
          />
          {
            this.state.username ? (
              <Card>
                  <CardItem header style={{backgroundColor:'#075e54', justifyContent:'center', alignContent:'center', alignItems:'center', alignSelf:'center', width:'100%'}}>
                      <Text style={{color:'white'}}>
                          Daftar Praktikum
                      </Text>
                  </CardItem>
                  <View style={{marginTop:20, marginBottom:20, marginLeft:10, marginRight:10}}>
                    {CardSatu}
                    {CardDua}
                    {CardTiga}
                    {CardEmpat}
                  </View>
                  
              </Card>
            ):  (
              <View>
      <View
        style={{
          justifyContent: 'center',
          alignContent: 'center',
          alignItems: 'center',
          marginTop: 50,
        }}
      >
        <Image
          source={require ('../../../assets/logo.jpg')}
          style={{width: '76%', resizeMode: 'contain'}}
        />
      </View>
      <Card style={{marginLeft: 30, marginRight: 30, marginTop: 70}}>
        <View
          style={{
            marginLeft: 20,
            marginRight: 20,
            marginTop: 30,
            marginBottom: 30,
          }}
        >
          <Item style={{borderBottomColor: 'black'}}>
            <Entypo name="user" size={20} color="black" />
            <Input
              placeholder="Username"
              placeholderTextColor="#000"
              style={{color: 'black'}}
              onChangeText={user => this.setState ({user})}
            />
          </Item>
          <Item style={{borderBottomColor: 'black'}}>
            <Entypo name="lock" size={20} color="black" />
            <Input
              placeholder="Password"
              placeholderTextColor="#000"
              style={{color: 'black'}}
              onChangeText={password => this.setState ({password})}
              secureTextEntry={true}
            />
          </Item>
          <View style={{marginTop: 20}}>
            <Button
              full
              style={{width: '100%', backgroundColor: '#075e54'}}
              onPress={this._UserLogin}
            >
              <Text style={{color: 'white'}}>Login</Text>
            </Button>
          </View>
        </View>
      </Card>
    </View>
            )
          }
              
          </Content>
          </ImageBackground>
        </Container>
      
      
    );
  }
}
