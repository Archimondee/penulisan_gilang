import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, AsyncStorage, ImageBackground, StatusBar} from 'react-native';
import {
  Header,
  Icon,
  Content,
  Container,
  Card,
  CardItem,
  Left,
  Right,
  Body,
} from 'native-base';
import {Ionicons} from '@expo/vector-icons';

export default class Page extends Component {
    constructor(props){
        super(props);
            this.state = {
                body: this.props.navigation.getParam('body'),
                image: this.props.navigation.getParam('image'),
                title: this.props.navigation.getParam('title')
            }
    }
    
    render(){

        return (
            <Container>
            <ImageBackground source={require('../../../assets/bg.jpg')} style={{height:"100%", width:"100%"}} resizeMode='cover'>
                <Header style={{backgroundColor:'#075e54', marginTop:StatusBar.currentHeight}}>
                <Left>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                        <Ionicons name="ios-arrow-back" size={40} color="white"/>
                    </TouchableOpacity>
                </Left>
                <Body>
                    <Text style={{color:'white', fontWeight:'bold', fontSize:15}}>{this.state.title}</Text>
                </Body>
                </Header>
                <Content padder>
                    <Card>
                        <CardItem>
                            <Image source={this.state.image}
                                style={{height:300, width:'100%'}}
                            />
                        </CardItem>
                        <CardItem style={{justifyContent:'center', alignContent:'center'}}>
                            <Text style={{fontSize:17}}>{this.state.title}</Text>
                        </CardItem>
                        <CardItem>
                            <Text style={{fontSize:14}}>
                                {this.state.body}
                            </Text>
                        </CardItem>
                        <CardItem >
                            <View style={{flexDirection:'column'}}>
                                <Text>
                                    Written By: Admin
                                </Text>
                                <Text>
                                    25 May 2017
                                </Text>
                            </View>
                            
                        </CardItem>
                    </Card>
                </Content>
                </ImageBackground>
            </Container>
        )
    }
}