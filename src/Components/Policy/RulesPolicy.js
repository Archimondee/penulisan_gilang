import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
} from 'react-native';
import {
  Content,
  Container,
  Card,
  CardItem,
  Body,
  Left,
  Header,
  Right,
} from 'native-base';
import {Ionicons} from '@expo/vector-icons';


export default class RulesPolicy extends Component {

  render() {
    return (
        
        <Container>
        <ImageBackground source={require('../../../assets/bg.jpg')} style={{height:"100%", width:"100%"}} resizeMode='cover'>
        <Header style={{backgroundColor:'#075e54', marginTop:StatusBar.currentHeight
        }}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={{marginTop:7}}>
                <Left>
                    <Ionicons name="ios-arrow-back" size={40} color="white"/>
                </Left>
            </TouchableOpacity>
            <Body style={{marginLeft:10}}>
                <Text style={{color:'white', fontWeight:'bold'}}>Peraturan Lab TI</Text>
            </Body>
        </Header>
        <Content padder>
            <Card style={{marginBottom:10}}>
                <CardItem header   style={{borderBottomWidth:1}}>
                    <Text>
                      Hadir 15 menit sebelum praktikum berlangsung. Seluruh mahasiswa Gunadarma yang aktif tahun ini maka untuk seluruh mata Praktikum(SKS + Non SKS/Penunjang) diwajibkan untuk hadir mengikuti praktikum yang ada sesuai praktikum yang diambil jika TIDAK HADIR dianggap GAGAL dalam praktikum tersebut.  
                    </Text>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection:'row'}}>
                        <Text>
                            1.
                        </Text>
                        <Text style={{paddingLeft: 5, flex:1}}>
                            Sopan dalam bertingkah laku dan bertutur kata
                        </Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection:'row'}}>
                        <Text>
                            2.
                        </Text>
                        <Text style={{paddingLeft: 5, flex:1}}>
                            Menjaga kebersihan dan ketertiban dilingkungan Lab TI
                        </Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection:'row'}}>
                        <Text>
                            3.
                        </Text>
                        <Text style={{paddingLeft: 5, flex:1}}>
                            Sebelum masuk ke ruangan praktikum tas,jaket, topi harus di buka dan diletakkan di loker yang telah disediakan. Duduk ditempat yang telah ditentukan melalui jalan yang berada ditengah ruangan.
                        </Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection:'row'}}>
                        <Text>
                            4.
                        </Text>
                        <Text style={{paddingLeft: 5, flex:1}}>
                            Tidak diperkenankan menggunakan perangkat music portable (Walkman,i-pod,walkmanphone,handphone,dll), makan,minum,dan merokok di lantai 4 dan di lantai 3
                        </Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection:'row'}}>
                        <Text>
                            5.
                        </Text>
                        <Text style={{paddingLeft: 5, flex:1}}>
                            Dilarang meninggalkan praktikum tanpa seijin ketua asisten atau asisten
                        </Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection:'row'}}>
                        <Text>
                            6.
                        </Text>
                        <Text style={{paddingLeft: 5, flex:1}}>
                            Kartu praktikum ditempel foto 2x3 formal di hekter di map praktikum
                        </Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection:'row'}}>
                        <Text>
                            7.
                        </Text>
                        <Text style={{paddingLeft: 5, flex:1}}>
                            MAP PLASTIK dibagi menjadi 3. Tingkat 1 & 2 berwarna putih (bening). Tingkat 3 berwarna hijau, dan tingkat 4 berwarna biru
                        </Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection:'row'}}>
                        <Text>
                            8.
                        </Text>
                        <Text style={{paddingLeft: 5, flex:1}}>
                            Laporan akhir harus di print tanpa di jilid
                        </Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection:'row'}}>
                        <Text>
                            9.
                        </Text>
                        <Text style={{paddingLeft: 5, flex:1}}>
                            Setiap praktikan bertanggung jawab atas komputer yang digunakannya. Tidak dibenarkan untuk berpindah tempat duduk dan berjalan jalan saat praktikum
                        </Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection:'row'}}>
                        <Text>
                            10.
                        </Text>
                        <Text style={{paddingLeft: 5, flex:1}}>
                            Wajib mengerjakan test pendahuluan sebelum praktikum berlangsung
                        </Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection:'row'}}>
                        <Text>
                            11.
                        </Text>
                        <Text style={{paddingLeft: 5, flex:1}}>
                            Menjaga peralatan praktikum Lab TI
                        </Text>
                    </Body>
                </CardItem>
                <CardItem>
                    <Body style={{flexDirection:'row'}}>
                        <Text>
                            12.
                        </Text>
                        <Text style={{paddingLeft: 5, flex:1}}>
                            Praktikan menghadiri 100% pertemuan atau praktikum memenuhi seluruh nilai ketika praktikum.
                        </Text>
                    </Body>
                </CardItem>
                
            </Card>
        </Content>
        </ImageBackground>
      </Container>
      
    );
  }
}
