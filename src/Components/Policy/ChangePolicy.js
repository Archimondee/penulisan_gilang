import React, {Component} from 'react';
import {View, Text, TouchableOpacity, StatusBar, ImageBackground} from 'react-native';
import {
  Content,
  Container,
  Card,
  CardItem,
  Body,
  Left,
  Header,
  Right,
} from 'native-base';
import {Ionicons} from '@expo/vector-icons';

export default class ChangePolicy extends Component {
  render () {
    return (
      
        <Container>
        <ImageBackground source={require('../../../assets/bg.jpg')} style={{height:"100%", width:"100%"}} resizeMode='cover'>
        <Header style={{backgroundColor:'#075e54', marginTop:StatusBar.currentHeight
        }}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={{marginTop:7}}>
                <Left>
                    <Ionicons name="ios-arrow-back" size={40} color="white"/>
                </Left>
            </TouchableOpacity>
            <Body style={{marginLeft:10}}>
                <Text style={{color:'white', fontWeight:'bold'}}>Petunjuk Ubah Password</Text>
            </Body>
        </Header>
        <Content padder>
          <Card>
            <CardItem header style={{borderBottomWidth: 0.5}}>
              <Text>Petunjuk Ubah Password</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  Ubahlah password secara berkala demi keamanan akun anda. Ingat kembali password yang telah anda ubah.
                </Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
        </ImageBackground>
      </Container>
      
    );
  }
}
