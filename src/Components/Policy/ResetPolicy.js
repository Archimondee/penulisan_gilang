import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
} from 'react-native';
import {
  Content,
  Container,
  Card,
  CardItem,
  Body,
  Left,
  Header,
  Right,
} from 'native-base';
import {Ionicons} from '@expo/vector-icons';


export default class ResetPolicy extends Component {
  render () {
    return (
      
        <Container>
        <ImageBackground source={require('../../../assets/bg.jpg')} style={{height:"100%", width:"100%"}} resizeMode='cover'>
        <Header style={{backgroundColor:'#075e54', marginTop:StatusBar.currentHeight
        }}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={{marginTop:7}}>
                <Left>
                    <Ionicons name="ios-arrow-back" size={40} color="white"/>
                </Left>
            </TouchableOpacity>
            <Body style={{marginLeft:10}}>
                <Text style={{color:'white', fontWeight:'bold'}}>Petunjuk Lupa Password</Text>
            </Body>
        </Header>
        <Content padder>
          <Card>
            <CardItem header style={{borderBottomWidth: 0.5}}>
              <Text>Petunjuk Lupa Password</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  Apabila anda mengalami lupa password silahkan datang ke staff. Setelah di berikan password yang baru, harap di ingat, jangan dilupakan kembali. Lupakan lah mantanmu, jangan passwordmu.
                </Text>
              </Body>
            </CardItem>
          </Card>
        </Content>
        </ImageBackground>
      </Container>
      
    );
  }
}
