import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  ImageBackground,
} from 'react-native';
import {
  Content,
  Container,
  Card,
  CardItem,
  Body,
  Left,
  Header,
  Right,
} from 'native-base';
import {Ionicons} from '@expo/vector-icons';


export default class WearPolicy extends Component {
  render() {
    return (
        
        <Container>
        <ImageBackground source={require('../../../assets/bg.jpg')} style={{height:"100%", width:"100%"}} resizeMode='cover'>
        <Header style={{backgroundColor:'#075e54', marginTop:StatusBar.currentHeight
        }}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()} style={{marginTop:7}}>
                <Left>
                    <Ionicons name="ios-arrow-back" size={40} color="white"/>
                </Left>
            </TouchableOpacity>
            <Body style={{marginLeft:10}}>
                <Text style={{color:'white', fontWeight:'bold'}}>Aturan Pakaian di Lab TI</Text>
            </Body>
        </Header>
        <Content padder>
           <Card>
            <CardItem header style={{borderBottomWidth:1, justifyContent:'center', alignContent:'center', alignItems:'center', alignSelf:'center', backgroundColor:'#075e54', width:'100%'}}>
                    <Text style={{color:'white'}}>
                        Wanita
                    </Text>
            </CardItem>
            <CardItem>
                <Body style={{flexDirection:'row'}}>
                    <Text>
                        1.
                    </Text>
                    <Text style={{paddingLeft: 5, flex:1}}>
                        Tidak Transparan
                    </Text>
                </Body>
            </CardItem>
            <CardItem>
                <Body style={{flexDirection:'row'}}>
                    <Text>
                        2.
                    </Text>
                    <Text style={{paddingLeft: 5, flex:1}}>
                        Tidak Ketat
                    </Text>
                </Body>
            </CardItem>
            <CardItem>
                <Body style={{flexDirection:'row'}}>
                    <Text>
                        3.
                    </Text>
                    <Text style={{paddingLeft: 5, flex:1}}>
                        Kancing ditutup hingga kancing kedua
                    </Text>
                </Body>
            </CardItem>
            <CardItem>
                <Body style={{flexDirection:'row'}}>
                    <Text>
                        4.
                    </Text>
                    <Text style={{paddingLeft: 5, flex:1}}>
                        Tidak menggunakan manset
                    </Text>
                </Body>
            </CardItem>
            <CardItem>
                <Body style={{flexDirection:'row'}}>
                    <Text>
                        5.
                    </Text>
                    <Text style={{paddingLeft: 5, flex:1}}>
                        Kemeja bukan berbahan jeans
                    </Text>
                </Body>
            </CardItem>
            <CardItem>
                <Body style={{flexDirection:'row'}}>
                    <Text>
                        6.
                    </Text>
                    <Text style={{paddingLeft: 5, flex:1}}>
                        Untuk lengan kemeja pendek harus 1 telapak tangan dari siku
                    </Text>
                </Body>
            </CardItem>
            <CardItem>
                <Body style={{flexDirection:'row'}}>
                    <Text>
                        7.
                    </Text>
                    <Text style={{paddingLeft: 5, flex:1}}>
                        Rok pada saat duduk berada dibawah lutut
                    </Text>
                </Body>
            </CardItem>
           </Card>
           <Card>
            <CardItem header style={{borderBottomWidth:1, justifyContent:'center', alignContent:'center', alignItems:'center', alignSelf:'center', backgroundColor:'#075e54', width:'100%'}}>
                    <Text style={{color:'white'}}>
                        Pria
                    </Text>
            </CardItem>
            <CardItem>
                <Body style={{flexDirection:'row'}}>
                    <Text>
                        1.
                    </Text>
                    <Text style={{paddingLeft: 5, flex:1}}>
                        Kemeja tidak boleh bermotif border
                    </Text>
                </Body>
            </CardItem>
            <CardItem>
                <Body style={{flexDirection:'row'}}>
                    <Text>
                        2.
                    </Text>
                    <Text style={{paddingLeft: 5, flex:1}}>
                        Tidak menggunakan kemeja flannel atau bermotif jeans
                    </Text>
                </Body>
            </CardItem>
            <CardItem>
                <Body style={{flexDirection:'row'}}>
                    <Text>
                        3.
                    </Text>
                    <Text style={{paddingLeft: 5, flex:1}}>
                        Celana bukan berbahan kodorai / celana kargo
                    </Text>
                </Body>
            </CardItem>
            <CardItem>
                <Body style={{flexDirection:'row'}}>
                    <Text>
                        4.
                    </Text>
                    <Text style={{paddingLeft: 5, flex:1}}>
                        Baju dimasukan kedalam celana
                    </Text>
                </Body>
            </CardItem>
            <CardItem>
                <Body style={{flexDirection:'row'}}>
                    <Text>
                        5.
                    </Text>
                    <Text style={{paddingLeft: 5, flex:1}}>
                        Rambut haruslah rapi
                    </Text>
                </Body>
            </CardItem>
            <CardItem>
                <Body style={{flexDirection:'row'}}>
                    <Text>
                        6.
                    </Text>
                    <Text style={{paddingLeft: 5, flex:1}}>
                        Bagi praktikan yang berambut panjang harus mengikat rambutnya
                    </Text>
                </Body>
            </CardItem>
           </Card>
            

           
        </Content>
        </ImageBackground>
      </Container>
      
    );
  }
}
