import React, {Component} from 'react';
import {View, Dimensions, StatusBar, ImageBackground} from 'react-native';

import {
  Container,
  Content,
  Button,
  Card,
  CardItem,
  Text,
  Body,
  Left,
  Right,
} from 'native-base';
import {Ionicons} from '@expo/vector-icons';

export default class PolicyScreen extends Component {
  render () {
    return (
      
        <Container>
        <ImageBackground source={require('../../../assets/bg.jpg')} style={{height:"100%", width:"100%"}} resizeMode='cover'>
        <Content padder>
          <Card style={{marginTop:StatusBar.currentHeight}}>
          <CardItem style={{justifyContent:'center', alignContent:'center', alignItems:'center', backgroundColor:'#075e54'}}>
            <Text style={{color:'white'}}>
                Policy
            </Text>
          </CardItem>
            <CardItem
              button
              onPress={()=>this.props.navigation.navigate('UsernamePolicy')}
            >
              <Left>
                <Text>Username & Password</Text>
              </Left>
              <Right>
                <Ionicons name="ios-arrow-dropright-outline" size={32} />
              </Right>
            </CardItem>
            <CardItem
              button
              onPress={()=>this.props.navigation.navigate('ChangePolicy')}
            >
              <Left>
                <Text>Tata Cara Ubah Password</Text>
              </Left>
              <Right>
                <Ionicons name="ios-arrow-dropright-outline" size={32} />
              </Right>
            </CardItem>
            <CardItem
              button
              onPress={()=>this.props.navigation.navigate('ResetPolicy')}
            >
              <Left>
                <Text>Tata Cara Lupa Password</Text>
              </Left>
              <Right>
                <Ionicons name="ios-arrow-dropright-outline" size={32} />
              </Right>
            </CardItem>
            <CardItem
              button
              onPress={()=>this.props.navigation.navigate('WearPolicy')}
            >
              <Left>
                <Text>Tata tertib berpakaian</Text>
              </Left>
              <Right>
                <Ionicons name="ios-arrow-dropright-outline" size={32} />
              </Right>
            </CardItem>
            <CardItem
              button
              onPress={()=>this.props.navigation.navigate('RulesPolicy')}
            >
              <Left>
                <Text>Peraturan Lab TI</Text>
              </Left>
              <Right>
                <Ionicons name="ios-arrow-dropright-outline" size={32} />
              </Right>
            </CardItem>
            <CardItem button>
              <Left>
                <Text>Author</Text>
              </Left>
              <Right>
                <Ionicons name="ios-arrow-dropright-outline" size={32} />
              </Right>
            </CardItem>
          </Card>

        </Content>
        </ImageBackground>
      </Container>
      
      
    );
  }
}
